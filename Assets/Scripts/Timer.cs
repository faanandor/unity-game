using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Timer : MonoBehaviour
{
    public GameObject textDisplay;
    public int seconds = 0;
    public bool addOn = false;

    void Start() 
    {
        textDisplay.GetComponent<Text>().text = "Time: " + seconds + " seconds";
    } 

    void Update() 
    {
        if (addOn == false) 
        {
            StartCoroutine(TimerTake());
        }
    }

    IEnumerator TimerTake () 
    {
        addOn = true;
        yield return new WaitForSeconds(1);
        seconds += 1;
        textDisplay.GetComponent<Text>().text = "Time: " + seconds + " seconds";
        addOn = false;
    }
}
