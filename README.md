# unity-game

Projekt név: Még nincs (frissítve lesz)

Mivel ez csak a második 3D játékprojektem, ezért nem szeretnék egy teljesen új ötlettel előállni. Úgy döntöttem, hogy egy parkour, mászkálós stílusú FPS játékot szeretnék kifejleszteni. Referenciaként/ihletként "Dani" nevű YouTuber Karlsson 3D nevű játékát szeretném használni. Hogy ne másolat legyen ezért én egy kicsit másfajta megközelítést szeretnék használni. A következő sorok lehet, hogy csak jött-ment brainstorming ötleteknek tűnnek majd, de mindenképp ezekből szeretnék kiindulni. 

A játék alaphelyszínét egy futurisztikus városként képzeltem el (Cyberpunk stílusban). Emellett, mindenképp egy éjszakai sötétebb környezet lenne, amit egy kicsit jobb megvilágítással fel lehetne dobni (neon fények, utcai lámpák). A játék hangulatát egy kis soundtrack-el szeretném feldobni, 80-as évekbeli stílusú retro zenével, szerintem jó párhuzam lenne a játék futurisztikus és retro tulajdonságai között. Ha sikerül megbarátkozni jobban a Blenderrel (korábban használtam), akkor egyéni karakter és animáció is lehetne a játékban. A parkour mechanikák között szeretném ha lehetséges lenne a falmászás, falfutás, lendületszerzés (sprint). Ezekre remélhetőleg tudok megfelelő dokumentációt találni. 

Egy egyszerűbb sztorira is gondoltam. A főhős mondjuk menekülhetne valahonnan, ez azt jelenti, hogy ellenséges AI-ra is szükség lesz, akik üldözik a főhőst. Támadó/harci mechanikákon még nem gondolkoztam, de lehet érdemes lenne legalább egy fegyvert a karakternek adni. Hangeffektekre mindenképp szükség lesz, viszont a párbeszédek lehet csak szöveg formájában jelennek majd meg. 

Szerintem az ötlet kivitelezhető, a közeljövőben dokumentálni fogom a játék fejlesztését, hogy hogyan haladok vele. 
